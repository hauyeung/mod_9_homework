﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Mod_9_Homework
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<Student> students;
        public int index;
        public MainWindow()
        {
            InitializeComponent();
            students = new List<Student>();
            index = 0;
        }

        private void btnCreateStudent_Click(object sender, RoutedEventArgs e)
        {
            Student student = new Student();
            student.firstName = txtFirstName.Text;
            student.lastName = txtLastName.Text;
            student.program = txtCity.Text;
            students.Add(student);
            txtFirstName.Clear();
            txtLastName.Clear();
            txtCity.Clear();
        }

        private void btnPrevious_Click(object sender, RoutedEventArgs e)
        {
            index--;
            txtFirstName.Text = students[index % students.Count].firstName;
            txtLastName.Text = students[index % students.Count].lastName;
            txtCity.Text = students[index % students.Count].program;
        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            index++;
            txtFirstName.Text = students[index % students.Count].firstName;
            txtLastName.Text = students[index % students.Count].lastName;
            txtCity.Text = students[index % students.Count].program;
        }
    }
}
